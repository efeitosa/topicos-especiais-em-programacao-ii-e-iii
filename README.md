# Tópicos Especiais em Programação II e III

Seja bem-vindo(a) à disciplina de Tópicos Especiais em Programação II e III, com foco em introdução ao sistema operacional Linux e à programação em shell script.

## Introdução ao Sistema Operacional Linux

1. [Conceitos básicos](https://gitlab.com/efeitosa/topicos-especiais-em-programacao-ii-e-iii/-/blob/main/Conceitos.md)
2. [Gerenciamento de diretórios e arquivos](https://gitlab.com/efeitosa/topicos-especiais-em-programacao-ii-e-iii/-/blob/main/Arquivos.md)
Gerenciamento de pacotes/aplicativos
Buscas de arquivos e diretórios
Gerenciamento de usuários e grupos
Gerenciamento de permissões (grupos, usuários, arquivos e diretórios)
Editores de texto
Gerenciamento básico de redes
Compactação e descompactação de arquivos e diretórios
Processos

## Introdução à Programação em Shell Script
