Nesta seção iremos apresentar alguns conceitos fundamentais como terminal e shell, estrutura de diretórios, além de explorar alguns comandos básicos do sistema operacional Linux.

O que é terminal e o que é shell?

**Terminal**:
O terminal, também conhecido como console ou emulador de terminal, é basicamente uma interface de texto que permite que você interaja com o sistema operacional por meio de comandos de texto. No contexto do Linux, você pode abrir um terminal para digitar comandos diretamente ao sistema.

**Shell**:
O shell é um programa que fornece uma interface entre o usuário e o núcleo do sistema operacional. Ele interpreta os comandos que você digita no terminal e os traduz para instruções compreensíveis pelo sistema operacional. No Linux, há diversos tipos de shells disponíveis, sendo o Bash (Bourne Again SHell) um dos mais comuns e populares.

Portanto, enquanto o terminal é a interface gráfica de linha de comando que você usa para inserir comandos, o shell é o programa que processa esses comandos e interage com o sistema operacional para executar as ações desejadas.

Para abrir o terminal:

1. Barra de tarefas e digitando terminal
2. Ctrl+Alt+t 

![Terminal](figures/terminal.png)

Como verificar a shell que você está usando:

1. Usando o comando ps
~~~
ps
~~~
2. Usando uma variável de ambiente
~~~
echo $SHELL
~~~

A shell indica que está pronta para receber comandos de entrada mostrando um prompt, normalmente indicado pelo símbolo $ (dolar)

Se a shell estiver esperando uma continuação do comando anterior, o prompt é modificado para um símbolo como > (maior)


Tipicamente os comandos seguem um padrão (sintaxe):

COMANDO  -[OPÇÕES]  [ARQUIVO/DIRETÓRIO]

Exemplo:
~~~
ls -l tmp/
~~~


### **Estrutura de Diretórios do Linux**

No Linux e Unix tudo é um arquivo. Diretórios são arquivos, arquivos são arquivos, e dispositivos são arquivos. 

Sistemas de arquivos do Linux e Unix são organizados em uma hierarquia, estilo estrutura de árvore. O nível mais alto do sistema de arquivos é o / ou diretório raiz. Todos os outros arquivos e diretórios existem sob o diretório root.

Sob o diretório root (/) existe um grupo de diretórios comuns à maioria das distribuições Linux. A seguinte lista contém uma lista dos diretórios comuns que estão diretamente sob o diretório root (/):

| Diretório | Conteúdo  |
| --------- | --------- |
| /bin      | binários executáveis usados para trabalhar com arquivos, textos e alguns recursos básicos de rede, como o cp, mv, ping e grep. |
| /sbin     | binários executáveis utilizados por administradores de sistema com o propósito de realizar funções de manutenção e outras tarefas semelhantes. Exemplo: ifconfig - usado para configurar interfaces de rede.  |
| /boot     | arquivos de configuração do boot, kernels, e outros arquivos necessários durante o período do boot.          |
| /dev      | os arquivos do dispositivo. Ao plugar um pendrive no computador, por exemplo, um arquivo será criado dentro do diretório /dev e ele servirá como interface para acessar ou gerenciar o drive.          |
| /etc      | arquivos de configuração e scripts de inicialização. Exemplo, o arquivo resolv.conf, com uma relação de servidores DNS que podem ser acessados pelo sistema. |
| /home     | diretórios home para diferentes usuários.     |
| /root     | diretório home do usuário root.          |
| /proc     | diretório dinâmico especial que mantém informação sobre o estado do sistema, incluindo os processos atualmente executados. Exemplo: Para saber há quanto tempo o Linux está sendo usado desde a última vez em que foi iniciado, basta ler o arquivo /proc/uptime.
| /lib      | bibliotecas de sistema. Bibliotecas usadas pelos comandos presentes em /bin e /sbin.   |
| /initrd   | usado quando está criando um processo de boot initrd personalizado.          |
| /media    | monta ( carrega ) automaticamente partições em seu disco rígido ou mídia removível como CDs, câmeras digitais, etc.          |
| /mnt      | sistema de arquivos montado manualmente em seu disco rígido.          |
| /sys      | arquivos de sistema.           |
| /srv      | pode conter arquivos que são servidos para outros sistemas. Exemplo: dados de servidores e serviços em execução no computador ficam armazenados dentro desse diretório.          |
| /tmp      | arquivos e diretórios criados temporariamente pelo sistema.          |
| /usr      | aplicativos e arquivos que são na maioria das vezes disponíveis ao acesso de todos usuários. O /usr reúne executáveis, bibliotecas e até documentação de softwares usados pelos usuários ou administradores do sistema. |
| /var      | arquivos variáveis tal como logs e bancos de dados          |



### **Comandos**

#### :penguin: **cd - altera de diretório**

Sintaxe: **cd [diretório]**

Exemplos:

1. Acesso direto
~~~
cd etc
~~~
~~~
cd /var/log
~~~
2. Ir direto para o diretório home
~~~
cd ~
~~~
3. Ir direto para o diretório raiz
~~~
cd /
~~~
4. Voltar um diretório
~~~
cd ..
~~~
5. Voltar mais de um diretório (exemplo: 3)
~~~
cd ../../../
~~~
6. Voltar para o último diretório
~~~
cd -
~~~
7. Sair e ir para um diretório específico
~~~
cd ../../var
~~~
8. Voltar mais de um diretório (exemplo: 3 diretórios)
~~~
cd ../../../
~~~
9. Exibir os diretórios existentes na pasta
~~~
cd Tecla TAB
~~~
10. Autocomplete
~~~
cd nomedir + TAB (duas vezes)
~~~
11. Encadeamento de comandos
~~~
cd /etc && ls
~~~
12. Informações de ajuda
~~~
cd --help
~~~


#### :penguin: **ls - lista o conteúdo de um diretório**

Sintaxe: **ls [opção] [arquivo/diretório]**

Exemplos:

1. Listar os arquivos/diretórios do diretório atual
~~~
ls
~~~
2. Listar os arquivos/diretórios de um diretório específico
~~~
ls /etc
~~~
3. Listar detalhes dos arquivos/diretórios
~~~
ls -l
~~~
![](figures/ls-l.png)

4. Listar incluindo os arquivos ocultos
~~~
ls -a
~~~
~~~
ls -la
~~~
5. Listar visualizando o tamanhos dos arquivos mais compreensíveis (human-readable)
~~~
ls -lh
~~~
6. Listar arquivos/diretórios de um diretório específico
~~~
ls -l /etc
~~~
7. Listar arquivos/diretórios ordenados por data de modificação 
~~~
ls -ltr
~~~
8. Listar subdiretórios
~~~
ls -R
~~~
9. Listar arquivos/diretórios ordenados por tamanho
~~~
ls -lS
~~~
10. Listar arquivos/diretórios separados por vírgula
~~~
ls -m
~~~
11. Informações de ajuda (resumido)
~~~
cd --help
~~~
12. Manual completo
~~~
man ls
~~~

#### :penguin: **clear - limpar a tela**

Sintaxe: **clear**
~~~
clear
~~~

#### :penguin: **cat - exibir o conteúdo de um arquivo ou criar um arquivo**

Sintaxe: **cat [opção]... arquivo...**

Exemplos:

1. Exibir o conteúdo de um arquivo
~~~
cat nome_do_arquivo
~~~
2. Exibir o conteúdo de mais de um arquivo
~~~
cat nome_do_arquivo1 nome_do_arquivo2 ...
~~~
3. Criar um arquivo com redirecionamento de sobrescrita
~~~
cat > nome_do_arquivo
~~~
Use ctrl+d para finalizar a entrada

~~~
cat nome_do_arquivo1 nome_do_arquivo2 ... > nome_do_arquivo
~~~

4. Criar um arquivo com redirecionamento de anexação
~~~
cat nome_do_arquivo1 nome_do_arquivo2 ... >> nome_do_arquivo
~~~

5. Exibir o conteúdo de um arquivo com número de linhas
~~~
cat -n nome_do_arquivo
~~~

6. Exibir o conteúdo de um arquivo exibindo caracteres não imprimíveis (fim de liha
~~~
cat -e nome_do_arquivo
~~~

7. Informações de ajuda (resumido)
~~~
cat --help
~~~
8. Manual completo
~~~
man cat
~~~

#### :penguin: **less e more - exibir o conteúdo de um arquivo**

less e more são outros dois comandos no Linux usados para exibir o conteúdo de um arquivo de texto no terminal. Embora ambos sirvam a um propósito semelhante, existem algumas diferenças 

- more: Permite apenas a navegação para baixo, uma tela de cada vez. Pressionar a tecla de espaço avança uma tela.

- less: Além da navegação para baixo, permite rolar para cima e pesquisar para frente e para trás. Oferece mais recursos de navegação interativa.

Sintaxe: **less [opção]... arquivo**

Sintaxe: **more [opção]... arquivo**

Exemplos:

1. Exibir o conteúdo de um arquivo
~~~
less nome_do_arquivo
~~~
~~~
more nome_do_arquivo
~~~

#### :penguin: **touch - criar um arquivo ou modificar a data de atualização (timestamp)**

Sintaxe: **touch [opção]... arquivo...**

Exemplos:

1. Criar um arquivo
~~~
touch nome_do_arquivo
~~~
2. Modificar da data do arquivo
~~~
touch nome_do_arquivo (quando o arquivo já existe) 
~~~
3. Criar múltiplos arquivos
~~~
touch nome_do_arquivo1 nome_do_arquivo2 nome_do_arquivo2 ...
~~~
4. Informações de ajuda (resumido)
~~~
touch --help
~~~
5. Manual completo
~~~
man touch
~~~

