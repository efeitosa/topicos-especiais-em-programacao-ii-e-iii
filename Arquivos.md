Nesta seção exploraremos os fundamentos essenciais do gerenciamento de arquivos e diretórios no sistema operacional Linux. Abordaremos tarefas como criar, excluir, copiar e mover arquivos e diretórios, fornecendo as bases necessárias para uma administração eficiente do sistema.

### **Criando Diretórios: :penguin: mkdir**

Sintaxe: **mkdir [opção]... diretório...**

Exemplos:

1. Criar diretório
~~~
mkdir nome_do_diretório
~~~
2. Criar múltiplos diretórios
~~~
mkdir nome_do_diretório1 nome_do_diretório2 nome_do_diretório3 ...
~~~
3. Visualizar a criação de diretórios
~~~
mkdir -v nome_do_diretório1 nome_do_diretório2 nome_do_diretório3 ...
~~~
-v = verbose

4. Criar um diretório dentro de outro diretório
~~~
mkdir nome_do_diretório1/nome_do_diretório2 
~~~
obs: diretório1 tem que existir

5. Criar estrutura de diretórios
~~~
mkdir -p nome_do_diretório1/nome_do_diretório2/nome_do_diretório3/...
~~~
-p = parents

6. Voltar e criar diretórios
~~~
mkdir ../nome_do_diretório
~~~
7. Informações de ajuda (resumido)
~~~
mkdir --help
~~~
8. Manual completo
~~~
man mkdir
~~~

### **Removendo Arquivos e Diretórios: :penguin: rm**

Sintaxe: **rm [opção]... [arquivo]...**

Exemplos:

1. Remover arquivo
~~~
rm nome_do_arquivo
~~~
2. Remover múltiplos arquivos
~~~
rm nome_do_arquivo1 nome_do_arquivo2 nome_do_arquivo3 ...
~~~
3. Remover arquivo com confirmação
~~~
rm -i nome_do_arquivo
~~~
-i = interactive (y=yes ou n=no)


O comando rm não foi projetado para remover diretório

![tentativa_de_remover_diretorio_com_rm](uploads/4dffe67d9aa5c6e8f10d9a73e6198e89/tentativa_de_remover_diretorio_com_rm.png)


4. Remover diretório vazio com rm
~~~
rm -dv nome_do_diretório 
~~~
-d = dir

5. Remover diretório com conteúdo com rm
~~~
rm -rfv nome_do_diretório 
~~~
-r = recursive

-f = force

6. Informações de ajuda (resumido)
~~~
rm --help
~~~
7. Manual completo
~~~
man rm
~~~

Por razões de segurança o Linux tem um comando específico para remover diretórios.

### **Removendo Diretórios: :penguin: rmdir**

Sintaxe: **rmdir [opção]... diretório...**

1. Remover diretório
~~~
rmdir nome_do_diretório
~~~
2. Remover diretório em cadeia
~~~
rmdir -p nome_do_diretório1/nome_do_diretório2/nome_do_diretório3/ ...
~~~
3. Informações de ajuda (resumido)
~~~
rmdir --help
~~~
4. Manual completo
~~~
man rmdir
~~~

### **Copiando Arquivos e Diretórios: :penguin: cp**

Sintaxe: **cp [opção]... source dest...**

1. Copiar arquivos 
~~~
cp nome_arq_fonte nome_arq_dest
~~~
2. Copiar arquivo para diretório
~~~
cp nome_arq_fonte nome_dir_dest/
~~~
3. Copiar múltiplos arquivos para diretório
~~~
cp nome_arq_fonte1 nome_arq_fonte2 nome_arq_fonte3 ... nome_dir_dest/
~~~
4. Copiar diretório para dentro de outro diretório
~~~
cp -r nome_dir_fonte nome_dir_dest
~~~
-r =recursive

5. Copiar todo o conteúdo do diretório para outro diretório
~~~
cp nome_dir_fonte/* nome_dir_dest 
~~~
- não copia os diretórios
- o diretório destino tem que estar criado

~~~
cp -r nome_dir_fonte/* nome_dir_dest 
~~~
- copia incluindo os diretórios

-r =recursive

6. Copiar vários arquivos com wildcard(*) para um diretório
~~~
cp doc* nome_dir_dest 
~~~
- nesse caso copia todos os arquivos cujo nome começa com "doc" para o diretório destino.
- diretório destino precisa estar criado.

7. Informações de ajuda (resumido)
~~~
cp --help
~~~
8. Manual completo
~~~
man cp
~~~

### **Movendo Arquivos e Diretórios: :penguin: mv**

Sintaxe: **mv [opção]... source dest**

1. Renomear arquivos 
~~~
mv nome_arq_original nome_arq_desejado
~~~
- o arquivo original deixa de existir

2. Mover arquivo para diretório
~~~
mv nome_arquivo nome_dir_dest/
~~~
3. Mover múltiplos arquivos para diretório
~~~
mv nome_arquivo1 nome_arquivo2 nome_arquivo3 ... nome_dir_dest/
~~~
4. Mover todos os arquivos para outro diretório
~~~
mv * nome_dir_dest
~~~

5. Informações de ajuda (resumido)
~~~
mv --help
~~~
6. Manual completo
~~~
man mv
~~~



